'use strict';

const http = require('http');
const fs = require('fs');
const querystring = require('querystring');
const port = process.env.PORT || 3000;
const base = './public';
const hostname = 'netology.tomilomark.ru';


function checkFile(filename) {
  return new Promise((resolve, reject) => {
      fs.stat(filename, (err, stat) => {
        if(err) return reject(err);
        if(stat.isDirectory()) {
          return resolve(checkFile(filename + 'index.html'));
        }
        if(!stat.isFile()) return reject('Not a file');
        fs.access(filename, fs.R_OK, err => {
            if(err) reject(err);
            resolve(filename);
        })
      });
    });
}

function parse(data, type) {
    switch (type) {
        case 'application/json':
            data = JSON.parse(data);
            break;
        case 'application/x-www-form-urlencoded':
            data = querystring.parse(data);
            break;
    }
    return data;
}



function handler(req, res) {

    let clientData = '';

    function process(data) {
        data = Object.assign(JSON.parse(data), JSON.parse(clientData));
        data = JSON.stringify(data);
        res.writeHead(200, 'OK', {'Content-Type': 'application/json', 'Content-Length': Buffer.byteLength(data)});
        res.write(data);
        res.end();
    }

    function handlerClient(response) {
        let data = '';
        response.on('data', chunk => {
            data += chunk;
        });
        response.on('end', () => {
            process(data);
        })
    }

    if(req.method == 'POST') {
        //console.log(req.url);
        // let data = '';
        req.on('data', chunk => clientData += chunk);
        req.on('end', () => {
            clientData = parse(clientData, req.headers['content-type']);

            const options = {
                hostname,
                path: '/api/v1/hash',
                method: 'POST',
                headers: {
                    firstname: clientData.first_name,
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(JSON.stringify(clientData))
                }
            };
            // заголовки и данные подготовлены - делаем запрос на удаленный сервер
            // реализуем клиент
            let request = http.request(options, handlerClient);
            clientData = JSON.stringify(clientData);
            request.write(clientData);
            request.end();

        });

        return;  // выходим из обработчика
    }

    checkFile(base + req.url)
        .then(filename => {
            res.writeHead(200, 'OK', {'Content-Type': 'text/html'});
            fs.createReadStream(filename).pipe(res);

        })
        .catch( err => {
            res.end('File not found ');
        })
}

const server = http.createServer();
server.on('error', err => console.error(err));
server.on('request', handler);
server.on('listening', () => {
  console.log('Start HTTP on port %d', port);
});
server.listen(port);
